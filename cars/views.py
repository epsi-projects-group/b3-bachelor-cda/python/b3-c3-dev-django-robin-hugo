from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from .models import Car, Rating
from .forms import RatingForm

# Create your views here.
def car_list(request):
    cars = Car.objects.all()
    return render(request, "cars/car_list.html", {"cars": cars})

def car_detail(request, car_id):
    car = get_object_or_404(Car, pk=car_id)
    return render(request, 'cars/car_detail.html', {"car": car})

@login_required
def car_rating(request, car_id):
    car = get_object_or_404(Car, pk=car_id)
    form = RatingForm(request.POST or None)
    if form.is_valid():
        print("Here in form")
        rating = form.save(commit=False)
        rating.car = car
        rating.user = request.user
        rating.save()
        print("Here")
        return redirect('car_detail', car_id=car.id)
    return render(request, 'cars/car_rating.html', {'car': car, 'form': form})

def car_ratings(request, car_id):
    car = get_object_or_404(Car, pk=car_id)
    ratings = Rating.objects.filter(car=car)
    return render(request, 'cars/car_ratings.html', {'car': car, 'ratings': ratings})