from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


# Create your models here.
class Car(models.Model):
    brand = models.CharField(max_length=100)
    model = models.CharField(max_length=100)
    year = models.PositiveIntegerField()
    engine_size = models.DecimalField(max_digits=4, decimal_places=1)
    version = models.CharField(max_length=50)
    
    def __str__(self):
        return f"{self.brand} - {self.model} - {self.version}"
    
class Rating(models.Model):
    car = models.ForeignKey(Car, related_name="ratings", on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name="ratings", on_delete=models.CASCADE)
    rating = models.DecimalField(max_digits=2, decimal_places=1, validators=[MinValueValidator(1), MaxValueValidator(5)])
    
    def __str__(self):
        return f"{self.user.username} - {self.rating}"