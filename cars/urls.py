from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.car_list, name='car_list'),
    path('<int:car_id>/', views.car_detail, name='car_detail'),
    path('<int:car_id>/rating/', views.car_rating, name='car_rating'),
    path('<int:car_id>/ratings/', views.car_ratings, name='car_ratings'),
]