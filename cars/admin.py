from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html
from .models import Car, Rating


class CarAdmin(admin.ModelAdmin):
    list_display = ('id', 'brand', 'model', 'year', 'version', 'view_on_site_link')
    list_filter = ('brand', 'model', 'year', 'version')
    search_fields = ('brand', 'model', 'year')
    ordering = ('brand', 'model')

    def view_on_site_link(self, obj):
        if obj.id:
            url = reverse('car_detail', args=[obj.id])
            return format_html('<a href="{}">View on site</a>', url)
        return '-'

    view_on_site_link.short_description = 'View on site'
    
# class CarAdmin(admin.ModelAdmin):
#     list_display = ('id', 'brand', 'model', 'year', 'version', 'view_on_site_link')
#     list_filter = ('brand', 'model', 'year', 'version')
#     search_fields = ('brand', 'model', 'year')
#     ordering = ('brand', 'model')

#     def view_on_site_link(self, obj):
#         if obj.id:
#             url = reverse('car_detail', args=[obj.id])
#             return format_html('<a href="{}">View on site</a>', url)
#         return '-'

#     view_on_site_link.short_description = 'View on site'


admin.site.register(Car, CarAdmin)
admin.site.register(Rating)